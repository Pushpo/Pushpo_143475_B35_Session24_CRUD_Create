<?php

namespace APP\ProfilePicture;
use App\Model\Database as DB;
require_once("../../../vendor/autoload.php");
use PDO;
class ProfilePicture extends DB
{
    public $id;
    public $name;
    public $profile_picture;
    public function __construct()
    {
        parent::__construct();
    }
    public function index()
    {
        echo "inside index of profile picture";
    }
}
$obj = new ProfilePicture();
